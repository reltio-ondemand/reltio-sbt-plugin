/*
 * Copyright 2016 Reltio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.reltio.tools

import java.io.{InputStream, FileInputStream, File}
import java.util.Properties
import java.util.regex.Pattern

import com.typesafe.config.ConfigFactory

import scala.io.Source
import scalaj.http.{Http, HttpOptions, HttpResponse}

import collection.JavaConverters._
/**
  * This class is the main execution entry point of the deployment script
  *
  * @author maxim.lukichev
  *
  * @param config the script configuration object
  */
class QuboleDeployScript(val config: QuboleConfig) {

  val awsClient = AwsClient(config)
  //mostly needed to mock aws client in unit tests easier
  def getAwsClient = awsClient

  def doDeploy() = {
    //1. upload jar file and ad-hoc script
    uploadFiles()
    //2. execute ad-hoc script
    executeAdHocScript()
    //3. modify bootstrap file
    modifyBootstrap()
    //4. restart zeppelin
    restartZeppelin()
  }

  def scriptUploadLocation = {
    val lp = config.qblLibPath
    val libPath = lp.endsWith("/") match {
      case true => lp.substring(0, lp.length - 1)
      case false => lp
    }
    libPath + "/" + config.qblClusterId + "/custom-script.sh"
  }

  def jarUploadLocation: Seq[(String,String)] = {
    config.jarFiles.map(file => {
      val fileName = new File(file).getName
      val lp = config.qblLibPath
      val libPath = lp.endsWith("/") match {
        case true => lp.substring(0, lp.length - 1)
        case false => lp
      }
      (file, libPath + "/" + fileName)
    })
  }

  def zeppelinUploadLocation = {
    val lp = config.qblLibPath
    val libPath = lp.endsWith("/") match {
      case true => lp.substring(0, lp.length - 1)
      case false => lp
    }
    libPath + "/" + config.qblClusterId + "/restart-zeppelin"
  }


  def scriptContent = {
    //val file = config.jarFile.get
    val scriptStream : InputStream = getClass.getResourceAsStream("/ad-hoc-script-template")
    val scriptTemplate = Source.fromInputStream(scriptStream).getLines().toArray.mkString("\n")
    scriptStream.close()

    val lineStream : InputStream = getClass.getResourceAsStream("/download-file-template")
    val lineTemplate  = Source.fromInputStream(lineStream).getLines().toArray.mkString("\n")

    val lines = jarUploadLocation.map(f => {
      lineTemplate.replaceAll("<file>", f._2)
    }).mkString("\n")

    scriptTemplate
      .replaceAll("<app>", config.appName)
      .replaceAll("<lines>", lines)
  }

  def zeppelinScriptContent = {
    val zeppelinStream : InputStream = getClass.getResourceAsStream("/restart-zeppelin")
    val zeppelinTemplate = Source.fromInputStream(zeppelinStream).getLines().toArray.mkString("\n")

    zeppelinTemplate.replaceAll("<file>", zeppelinUploadLocation)
  }

  def uploadFiles() = {
    println("Uploading jar and ad-hoc script file...")

    //upload script
    getAwsClient.updateFileContent(scriptContent, scriptUploadLocation)
    //upload jar files
    jarUploadLocation.foreach(x => getAwsClient.uploadFile(x._1, x._2))
    //upload zeppelin script
    getAwsClient.updateFileContent(zeppelinScriptContent, zeppelinUploadLocation)

    println("Finished uploading jar and ad-hoc script file.")
  }

  def executeAdHocScript() = {
    println("Submitting ad-hoc script...")

    val data = "{\"script\":\"" + scriptUploadLocation + "\"}"

    val response = doSendRequest(qblUrl, config.qblApiKey, data)

    val code = response.code
    if (code != 200)
      throw new RuntimeException(s"Script submission failed with response status: $code and message: " + response.body)

    println("Finished submitting ad-hoc script: " + response.body)
  }

  def restartZeppelin(): Unit = {
    if (!config.restart) {
      return
    }

    println("Finished uploading restart zeppelin command. Submitting restart zeppelin command...")

    val data = "{\"script\":\"" + zeppelinUploadLocation + "\"}"

    val response = doSendRequest(qblUrl, config.qblApiKey, data)

    val code = response.code
    if (code != 200)
      throw new RuntimeException(s"Restart failed with response status: $code and message: " + response.body)

    println("Submitted restart zeppelin command: " + response.body)
  }

  def doSendRequest(url: String, authToken: String, data: String) = {
    val executeScript: HttpResponse[String] = Http(url)
      .headers(
        ("Content-Type", "application/json"),
        ("X-AUTH-TOKEN", authToken),
        ("Accept", "application/json"))
      .option(HttpOptions.allowUnsafeSSL)
      .postData(data)
      .method("PUT")
      .asString

    executeScript
  }

  def modifyBootstrap(): Unit = {
    if (config.qblBootstrapFile.isEmpty){
      return
    }

    println("Updating bootstrap script...")

    val script = getAwsClient.downloadFile(config.qblBootstrapFile.get)
    getAwsClient.updateFileContent(replaceJars(script, scriptContent), config.qblBootstrapFile.get)

    println("Finished updating bootstrap script")
  }

  def replaceJars(script: String, replacement: String) = {
    val start = "#<app>:".replaceAll("<app>", config.appName)
    val end = "#end of <app>".replaceAll("<app>", config.appName)
    val pattern = s"\\s*$start.*?$end"

    val newScript = Pattern.compile(pattern, Pattern.DOTALL).matcher(script).replaceAll("")

    newScript + "\n\n" + replacement
  }

  def qblUrl: String = {
    val conf = ConfigFactory.load(QuboleDeployScript.getClass.getClassLoader, "application.conf")
    conf.getString("conf.qubole.url").replaceFirst("<cluster-id>", config.qblClusterId)
  }

}

object QuboleDeployScript {

  def apply(config: QuboleConfig) = {
    new QuboleDeployScript(config)
  }

  def main(args: Array[String]) = {
    require(args.size > 0 && new File(args(0)).exists(), "Require input properties file")
    val prop = new Properties()
    prop.load(new FileInputStream(new File(args(0))))

    val configMap = prop.stringPropertyNames().asScala.map(x => (x, prop.getProperty(x))).toMap

    val config = QuboleConfig(configMap)
    val deploy = new QuboleDeployScript(config)

    //execute action
    deploy.doDeploy()
  }
}
