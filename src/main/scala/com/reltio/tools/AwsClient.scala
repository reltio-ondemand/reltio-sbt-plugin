/*
 * Copyright 2016 Reltio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.reltio.tools

import java.io.File

import com.amazonaws.{AmazonServiceException, AmazonClientException}
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client

/**
 *
 * Simple client to abstract operations with AWS S3
 *
 * @author maxim.lukichev
 */
class AwsClient(val config: QuboleConfig) {

  private lazy val userAwsCredentials = new BasicAWSCredentials(config.awsKey, config.awsSecret)
  private lazy val userS3Client = new AmazonS3Client(userAwsCredentials)
  userS3Client.setRegion(com.amazonaws.regions.Region.getRegion(Regions.US_EAST_1))

  def bucketName(url: String): String = {
    //s3://test.bucket/folder1/
    val s = url.replaceFirst("s3://", "")
    s.substring(0, s.indexOf("/"))
  }

  def fileLoc(url: String): String = {
    url.replaceFirst("s3://", "").replaceFirst(bucketName(url) + "/", "")
  }

  def awsCommand(resource: String, f : => Option[Any]): Option[Any] = {
    try {
      f
    } catch {
      case e: AmazonClientException =>
        println("Problem accessing AWS: " + e.getMessage)
        throw e
      case e: AmazonServiceException =>
        e.getStatusCode match {
          case 404 => println(s"[$resource] doesn't exist")
          case 403 => println(s"User doesn't have permissions to [$resource]")
        }
        throw e
    }
  }

  def prepareUpload(url: String ) = {
    val bucket = bucketName(url)
    val fileKey = fileLoc(url)

    //check if bucket exists
    awsCommand(
    "bucket",
    {
      if (!userS3Client.doesBucketExist(bucket)) {
        userS3Client.createBucket(bucket)
      }
      None
    })

    //check if object exists and delete it if so
    awsCommand(
    "object",
    {
      if (userS3Client.doesObjectExist(bucket, fileKey)) {
        userS3Client.deleteObject(bucket, fileKey)
      }
      None
    })
  }

  /**
   * Update file's content on S3 bucket
   * @param content the file's content
   * @param url the file's location on S3
   * @return
   */
  def updateFileContent(content: String, url: String ) = {
    val bucket = bucketName(url)
    val fileKey = fileLoc(url)

    prepareUpload(url)

    //upload new file
    awsCommand(
    "object", {
      userS3Client.putObject(bucket, fileKey, content)
      None
    })
  }

  def uploadFile(file: String, url: String) = {
    val bucket = bucketName(url)
    val fileKey = fileLoc(url)

    prepareUpload(url)

    //upload new file
    awsCommand(
    "object", {
      userS3Client.putObject(bucket, fileKey, new File(file))
      None
    })
  }

  def downloadFile(url: String): String = {
    val bucket = bucketName(url)
    val fileKey = fileLoc(url)

    awsCommand(
    "object", {
      Option(userS3Client.getObjectAsString(bucket, fileKey))
    }).get.toString
  }

  def deleteFile(url: String) = {
    val bucket = bucketName(url)
    val fileKey = fileLoc(url)

    awsCommand(
    "object", {
      userS3Client.deleteObject(bucket, fileKey)
      None
    })
  }
}

object AwsClient {
  def apply(config: QuboleConfig) = {
    new AwsClient(config)
  }
}
