/*
 * Copyright 2016 Reltio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.reltio.tools

import com.reltio.tools.PropertyValidator._

/**
 * Configuration object for deploy tool
 *
 * @author maxim.lukichev
 *
 */
class QuboleConfig private[tools] (
                    val appName: String,
                    val awsKey: String,
                    val awsSecret: String,
                    val jarFiles: Seq[String],
                    val qblClusterId: String,
                    val qblApiKey: String,
                    val qblLibPath: String,
                    val qblBootstrapFile: Option[String],
                    val restart: Boolean = false
                    ) {
  override def toString: String = {
        "{" +
        s"  appName: $appName," +
        s"  awsKey: $awsKey," +
        s"  awsSecret: $awsSecret," +
        s"  jarFiles: $jarFiles," +
        s"  qblClusterId: $qblClusterId," +
        s"  qblApiKey: $qblApiKey," +
        s"  qblLibPath: $qblLibPath," +
        s"  qblBootstrapFile: $qblBootstrapFile," +
        s"  restart: $restart," +
        "}"
  }

}

object QuboleConfig {

  def apply(map: Map[String, String]) = {
    validateAndBuild(map)
  }

  def validateAndBuild(map: Map[String, String]) = {
    val context = new ValidationContext()

    val validators = Map(
      "appName" -> NonEmptyPropertyVaildator("appName", context),
      "awsKey" -> NonEmptyPropertyVaildator("awsKey", context),
      "awsSecret" -> NonEmptyPropertyVaildator("awsSecret", context),
      "qblClusterId" -> NonEmptyPropertyVaildator("qblClusterId", context),
      "qblApiKey" -> NonEmptyPropertyVaildator("qblApiKey", context),
      "qblLibPath" -> NonEmptyPropertyVaildator("qblLibPath", context),
      "jarFiles" -> MultipleFilesPropertyVaildator("jarFiles", context),
      "restart" -> BooleanPropertyVaildator("restart", context),
      "qblBootstrapFile" -> OptionPropertyVaildator("qblBootstrapFile", context)
    )

    validators.foreach(v => {
      val validator = v._2
      validator.validate(map.get(validator.propertyNameValue))
    })

    val values = validators.map(v => {
      val validator = v._2
      val rawValue = map.get(v._1)
      validator.processValue(rawValue)
    })

    require(context.failureCount == 0, "Configuration is not valid: \n" + context.toString)
    println("Configuration is valid. Continue processing...")

    new QuboleConfig(
      appName = validators.get("appName").get.processValue(map.get("appName")).asInstanceOf[String],
      awsKey = validators.get("awsKey").get.processValue(map.get("awsKey")).asInstanceOf[String],
      awsSecret = validators.get("awsSecret").get.processValue(map.get("awsSecret")).asInstanceOf[String],
      jarFiles = validators.get("jarFiles").get.processValue(map.get("jarFiles")).asInstanceOf[Seq[String]],
      qblClusterId = validators.get("qblClusterId").get.processValue(map.get("qblClusterId")).asInstanceOf[String],
      qblApiKey = validators.get("qblApiKey").get.processValue(map.get("qblApiKey")).asInstanceOf[String],
      qblLibPath = validators.get("qblLibPath").get.processValue(map.get("qblLibPath")).asInstanceOf[String],
      qblBootstrapFile = validators.get("qblBootstrapFile").get.processValue(map.get("qblBootstrapFile")).asInstanceOf[Option[String]],
      restart = validators.get("restart").get.processValue(map.get("restart")).asInstanceOf[Boolean]
    )
  }
}

