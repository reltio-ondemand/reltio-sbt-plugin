/*
 * Copyright 2016 Reltio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.reltio.tools

import java.io.File

/**
 * Collection of property validators
 *
 * @author maxim.lukichev
 */

object PropertyValidator {
  sealed abstract class AbstractPropertyValidator(
                                           val validateFunction: Option[String] => Boolean,
                                           val propertyNameValue: String,
                                           val context: ValidationContext) {
    def validate(value: Option[String]) = {
      if (!validateFunction(value)) {
        context += errorMessage
      }
    }

    def processValue(value: Option[String]): Any
    def errorMessage: String
  }

  case class OptionPropertyVaildator(pName: String, c: ValidationContext)
    extends AbstractPropertyValidator(dummyPropFunction, pName, c) {

    override def errorMessage: String = {
      throw new UnsupportedOperationException
    }

    override def processValue(value: Option[String]): Any = {
      value
    }
  }

  case class NonEmptyPropertyVaildator(pName: String, c: ValidationContext)
    extends AbstractPropertyValidator(nonemptyPropFunction, pName, c) {

    override def errorMessage: String = {
      val name = pName
      s"$name must be defined"
    }

    override def processValue(value: Option[String]): Any = {
      value.get
    }
  }

  case class MultipleFilesPropertyVaildator(pName: String, c: ValidationContext)
    extends AbstractPropertyValidator(nonemptyPropFunction, pName, c) {
    override def errorMessage: String = {
      val name = pName
      s"$name must be defined"
    }

    override def processValue(value: Option[String]): Any = {
      if (multipleFilesPropFunction(value)){
        files(value.get).toSeq
      } else {
        Seq[String]()
      }
    }
  }

  case class BooleanPropertyVaildator(pName: String, c: ValidationContext)
    extends AbstractPropertyValidator(isbooleanPropFunction, pName, c) {
    override def errorMessage: String = {
      val name = pName
      s"$name must be defined"
    }

    override def processValue(value: Option[String]): Boolean = {
      if (isbooleanPropFunction(value)){
        value.getOrElse("false").toBoolean
      } else {
        false
      }
    }
  }

  def dummyPropFunction(prop: Option[String]): Boolean = {
    true
  }

  def nonemptyPropFunction(prop: Option[String]): Boolean = {
      prop.isDefined
  }

  def multipleFilesPropFunction(jarFiles: Option[String]): Boolean = {
    jarFiles match {
      case x if x.isEmpty => false //jar files must be non empty
      case y if y.isDefined && !filesExist(files(y.get)) => false //jar files must exist
      case x => true //files(x.get).toSeq
    }
  }

  def files(s: String) = {
    s.split(",")
  }

  def filesExist(files: Array[String]) = {
    files.forall(new File(_).exists())
  }

  def isbooleanPropFunction(prop: Option[String]): Boolean = {
    val v = prop.getOrElse("false")
    try {
      v.toBoolean
      return true
    } catch {
      case e: Throwable => {
        return false
      }
    }
  }
}



