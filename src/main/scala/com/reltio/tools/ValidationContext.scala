/*
 * Copyright 2016 Reltio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.reltio.tools

/**
 * Validation context used to accumulate errors during validation of deployment properties
 *
 * @author maxim.lukichev
 */
class ValidationContext(private var errors: List[String] = List()) {

  def failureCount = errors.size

  def += (error: String): Unit = {
    errors ::= error
  }

  override def toString = {
    val errorList = "[" + errors.mkString(",\n") + "]"
    val failures  = errors.size

    s"""
       |totalFailures = $failures
       |errors: $errorList
     """.stripMargin
  }
}
