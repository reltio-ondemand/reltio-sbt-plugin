/*
 * Copyright 2016 Reltio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.reltio.tools

import sbt._
import Keys._
import java.io.File

/**
 * The plugin provides sbt command to deploy and attach ad-hoc scala libraries onto Qubole Spark clusters
 *
 * @author maxim.lukichev
 */
object QuboleDeployPlugin extends AutoPlugin {

  override def requires = plugins.JvmPlugin
  override def trigger = allRequirements

  val vAwsKey = System.getProperty("aws.key")
  val vAwsSecret = System.getProperty("aws.secret")
  val vApiKey = System.getProperty("qbl.api.key")

  object autoImport {
    val qblClusterId = settingKey[String]("Id of the cluster to upload your jar")
    val qblBootstrapFile = settingKey[String]("An optional parameter to set location of cluster's " +
     "bootstrap file. If it is specified, then the bootstrap file will be modified to deploy your jar")
    val qblLibraryPath = settingKey[String]("A location on S3 to upload your jar to. Note, the location should be " +
     "accessible by Qubole")
    val qblRestart = settingKey[Boolean]("If true, then restart Zeppelin on the cluster")
    val qblDeploy = taskKey[Unit]("Deploy jar file to Qubole cluster")
    val qblJarFiles = settingKey[Seq[String]]("The jar files to upload to Qubole cluster")
  }

  import autoImport._

  def artifactFiles(files: Seq[String]): Seq[String] = {
    files.map(x => {
      x match {
        case s if !s.contains(File.separator) => "target/scala-2.10/" + s //append target directory path
        case s => s
      }
    })
  }

  def print(s: QuboleConfig): Unit = {
    println(s"value:$s")
  }

  private lazy val doDeploy: Def.Initialize[Task[Unit]] = Def.task{

      val vClusterId = qblClusterId.value
      val vBootstrapFile = Option(qblBootstrapFile.value)
      val vLibPath = qblLibraryPath.value
      val vRestart = qblRestart.value
      val vJarFiles = artifactFiles(qblJarFiles.value)
      val vName = Keys.organization.value + "." + Keys.name.value

      val config = new QuboleConfig(
        appName = vName,
        awsKey = vAwsKey,
        awsSecret = vAwsSecret,
        jarFiles = vJarFiles,
        qblClusterId = vClusterId,
        qblApiKey = vApiKey,
        qblBootstrapFile = vBootstrapFile,
        qblLibPath = vLibPath,
        restart = vRestart)

      print(config)
      val script = QuboleDeployScript(config)
      script.doDeploy

  }

  val baseSettings: Seq[Setting[_]] = Seq(
    qblClusterId := {
      sys.error("qblClusterID not defined")
    },
    qblLibraryPath := {
      sys.error("qblLibraryPath not defined")
    },
    qblDeploy := doDeploy.value,
    qblRestart := {false}
  )

  override lazy val projectSettings: Seq[Setting[_]] = baseSettings

}