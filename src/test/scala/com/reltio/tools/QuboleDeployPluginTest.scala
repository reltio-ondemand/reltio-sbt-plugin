package com.reltio.tools

import org.scalatest.FlatSpec

class QuboleDeployPluginTest extends FlatSpec {

  "artifactFiles method" should "parse empty jar file correctly" in {
    QuboleDeployPlugin.artifactFiles(Seq())
  }

  "artifactFiles method" should "parse single jar file correctly" in {
    assert(
      QuboleDeployPlugin.artifactFiles(Seq("my.jar"))(0) == "target/scala-2.10/my.jar"
    )
    assert(
      QuboleDeployPlugin.artifactFiles(Seq("path/to/my.jar"))(0) == "path/to/my.jar"
    )
  }

  it should "parse list of jar files correctly" in {
    assert(
      QuboleDeployPlugin.artifactFiles(Seq("my/1.jar","my/2.jar")).size == 2
    )
  }
}