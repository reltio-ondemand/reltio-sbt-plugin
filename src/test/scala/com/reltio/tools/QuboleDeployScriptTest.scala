package com.reltio.tools

import org.mockito.Matchers._
import org.mockito.Mockito._
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer
import org.scalatest.FlatSpec

import scalaj.http.HttpResponse

class QuboleDeployScriptTest extends FlatSpec {
  val defaultConfig = new QuboleConfig(
    "myApp",
    "awsKey",
    "awsSecret",
    Seq("abc/def/test.file"),
    "123",
    "qblKey",
    "s3://test.bucket/test/",
    Option("s3://user.bucket/bootstrap.sh"),
    false
  )

  "jarUploadLocation" should "return correct path" in {
    var config = new QuboleConfig(
      defaultConfig.appName,
      defaultConfig.awsKey,
      defaultConfig.awsSecret,
      Seq("abc/def/test.file"),
      defaultConfig.qblClusterId,
      defaultConfig.qblApiKey,
      "s3://test.bucket/test/",
      defaultConfig.qblBootstrapFile,
      defaultConfig.restart
    )
    assert(new QuboleDeployScript(config).jarUploadLocation(0)._2 == "s3://test.bucket/test/test.file")

    config = new QuboleConfig(
      defaultConfig.appName,
      defaultConfig.awsKey,
      defaultConfig.awsSecret,
      Seq("abc/def/test.file"),
      defaultConfig.qblClusterId,
      defaultConfig.qblApiKey,
      "s3://test.bucket/test",
      defaultConfig.qblBootstrapFile,
      defaultConfig.restart)
    assert(new QuboleDeployScript(config).jarUploadLocation(0)._2 == "s3://test.bucket/test/test.file")
  }

  "scriptContent" should "return non-empty content" in {
    assert(new QuboleDeployScript(defaultConfig).scriptContent.nonEmpty)
  }

  "executeAdHocScript" should "build and send correct request" in {
    val qds = spy(new QuboleDeployScript(defaultConfig))

    val doSendRequestAnswer = new Answer[HttpResponse[String]] {
      override def answer(invocation: InvocationOnMock): HttpResponse[String] = {
        val url = invocation.getArgumentAt(0, classOf[String])
        val authToken = invocation.getArgumentAt(1, classOf[String])
        val data = invocation.getArgumentAt(2, classOf[String])

        val cluster = defaultConfig.qblClusterId

        assert(url == "https://api.qubole.com/api/v1.3/clusters/<cluster-id>/runscript.json".replaceAll("<cluster-id>", cluster))
        assert(authToken.nonEmpty)
        assert(data == "{\"script\":\"s3://test.bucket/test/123/custom-script.sh\"}")

        new HttpResponse[String]("", 200, Map())
      }
    }
    doAnswer(doSendRequestAnswer).when(qds).doSendRequest(anyString(), anyString(), anyString())

    qds.executeAdHocScript()
  }

  "replaceJars" should "add new app section when no previous exists" in {
    val qds = spy(new QuboleDeployScript(defaultConfig))
    val replacement =
      """#myApp: downloading custom jars
        |something
        |#end of myApp""".stripMargin

    val script = "my script"

    val res = qds.replaceJars(script, replacement)
    assert(res ==
      s"""my script
        |
        |$replacement""".stripMargin)
  }

  it should "replace existing app section with the new one" in {
    val qds = spy(new QuboleDeployScript(defaultConfig))
    val app =
      """#myApp: downloading custom jars
        |something
        |#end of myApp""".stripMargin

    val replacement =
      """#myApp: downloading custom jars
        |something else
        |#end of myApp""".stripMargin

    val script =
      s"""my script
         |
         |$app""".stripMargin

    val res = qds.replaceJars(script, replacement)
    assert(res ==
      s"""my script
         |
         |$replacement""".stripMargin)

  }
}
