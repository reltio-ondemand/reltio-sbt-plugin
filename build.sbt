sbtPlugin := true

name := "sbt-qbl"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.10.4"

organization := "com.reltio"

version in ThisBuild := s"${version.value}"

organization in ThisBuild := s"${organization.value}"

licenses += ("Apache-2.0", url("https://www.apache.org/licenses/LICENSE-2.0.html"))

//ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }

libraryDependencies ++= Seq(
  "com.amazonaws" % "aws-java-sdk" % "1.11.20",
  "org.scalactic" %% "scalactic" % "3.0.1" % "test",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "org.mockito" % "mockito-all" % "1.10.19" % "test",
  "org.scalaj" %% "scalaj-http" % "2.3.0",
  "com.typesafe" % "config" % "1.3.0",
  "org.scala-lang" % "scala-library" % scalaVersion.value % "provided"
)

retrieveManaged := true

assemblyJarName in assembly := "qbl-deploy-all.jar"

publishTo := Some(Resolver.file("file",  new File(Path.userHome.absolutePath+"/.m2/repository")))

publishMavenStyle := false
